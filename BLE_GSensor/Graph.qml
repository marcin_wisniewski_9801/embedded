import QtQuick 2.0
import QtCharts 2.15

Item {
    id: root
    property int maxX: 100
    property int maxY: 100
    property int tickX: 5
    property int tickY: 5
    property int minorTickX: 4
    property int minorTickY: 4
    property string mainTitle: ""
    property string titleX: ""
    property string titleY: ""
    property bool useOpenGL: false
    ChartView {
        id: line
        anchors.fill: parent
        theme: ChartView.ChartThemeDark
        title: root.mainTitle
        ValueAxis {
            id: axisX
            min: 0
            max: root.maxX
            tickCount: root.tickX
            minorTickCount: root.minorTickX
            labelFormat: "%.0f"
            titleText: root.titleX
        }
        ValueAxis {
            id: axisY
            min: -root.maxY
            max: root.maxY
            tickCount: tickY
            minorTickCount: root.minorTickY
            labelFormat: "%.0f"
            titleText: root.titleY
        }
        LineSeries {
            id: seriesX
            name: "X"
            color: "red"
            axisX: axisX
            axisY: axisY
            useOpenGL: root.useOpenGL
        }
        LineSeries {
            id: seriesY
            name: "Y"
            color: "lime"
            axisX: axisX
            axisY: axisY
            useOpenGL: root.useOpenGL
        }
        LineSeries {
            id: seriesZ
            name: "Z"
            color: "cyan"
            axisX: axisX
            axisY: axisY
            useOpenGL: root.useOpenGL
        }
    }
    function dataClear() {
        seriesX.clear();
        seriesY.clear();
        seriesZ.clear();
    }
    function dataUpdate(x, y, z) {
        if(seriesX.count>axisX.max)
        {
            seriesX.clear();
            seriesY.clear();
            seriesZ.clear();
        }
        seriesX.append( seriesX.count, x)
        seriesY.append( seriesY.count, y)
        seriesZ.append( seriesZ.count, z)
    }
}
