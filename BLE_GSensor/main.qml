import QtQuick 2.15
import QtQuick.Controls 2.15

ApplicationWindow {
    id: applicationWindow
    width: 800
    height: 600
    visible: true
    title: qsTr("G-Sensor")
    header: ToolBar {
        contentHeight: toolButton.implicitHeight
        Row{
            id: row1
            ToolButton {
                id: toolButton
                text: "\u2630"
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                onClicked: {
                    scanButton.enabled=true;
                    scanButton.text="Scan"
                    drawer.open()
                }
            }
            ToolButton {
                id: toolButton2
                text: "\u2699"
                font.pixelSize: Qt.application.font.pixelSize * 2.0
                enabled: false
                onClicked: {
                    drawer2.open()
                }
            }
        }
        Label {
            id: frateText
            anchors.right: battText.left
            anchors.rightMargin: 10
            y:16
            text: ""
        }
        Image {
         id: batteryIcon
         source: "icons/batt.png"
         y:6
         anchors.right: parent.right
         anchors.rightMargin: 10
         }
        Rectangle{
           id: batteryIconFill
           y:18
           anchors.right: parent.right
             anchors.rightMargin: 15
             color: "#FF0000"


             height: 13

             width: 28

        }
         Label {
         id: battText
         anchors.right: batteryIcon.left
         anchors.rightMargin: 6
         y:16
         text: "---%"
         }

    }
    Drawer {
        id: drawer
        width: 250
        height: applicationWindow.height
        Button {
            id: scanButton
            width: parent.width
            text: "Scan"
            onClicked: {
                text="Scanning..."
                listView.enabled=false
                busyIndicator.running=true;
                enabled = false;
                bledevice.startScan()
            }
        }
        ListView {
            id: listView
            anchors.fill: parent
            anchors.topMargin: 50
            anchors.bottomMargin: 50
            width: parent.width
            clip: true
            model: bledevice.deviceListModel
            delegate: RadioDelegate {
                id: radioDelegate
                text: (index+1)+". "+modelData
                width: listView.width
                onCheckedChanged: {
                    console.log("checked", modelData, index)
                    scanButton.enabled=false;
                    scanButton.text="Connecting to "+modelData
                    listView.enabled = false;
                    bledevice.startConnect(index)
                }
            }
        }
        BusyIndicator {
            id: busyIndicator
            anchors.centerIn: parent
            running: false
        }
    }

    Drawer {
        id: drawer2
        width: 250
        height: applicationWindow.height
        Button {
            id: setButton
            width: parent.width
            text: "Set"
            onClicked: {
                text=""
                listView.enabled=false
                busyIndicator.running=true;
                enabled = false;

            }
        }

        BusyIndicator {
            id: busyIndicator2
            anchors.centerIn: parent
            running: false
        }

    }
    Graph {
        id: graph
        useOpenGL: true;
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: parent.height/2
        mainTitle: "Accelerometer "
        titleX: "Time [smp]"
        titleY: "a [\u33A8]"
        maxX: 1000
        maxY: 100
        tickY: 5
        minorTickY: 4
    }
    Graph {
        id: graph2
        useOpenGL: true;
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        height: parent.height/2
        mainTitle: "Magnetometer"
        titleX: "Time [smp]"
        titleY: "B [\u00B5T]"
        maxX: 1000
        maxY: 1200
        tickY: 5
        minorTickY: 5
    }
    Timer {
        id: refreshTimer
        interval: 5000
        repeat: true
        triggeredOnStart: true
        onTriggered: {
            var batt = 0
            batt = bledevice.getBatteryLevel();
            battText.text=+batt+"%"
        }
    }




    Connections {
        target: bledevice
        function onNewData(data) {
            if(data.length === 7) {
                frateText.text = "FR: "+data[6]
                if(data[6]===1) {
                    frateText.color="white"
                    toolButton2.enabled=true
                } else {
                    frateText.color="red"
                }
                graph.dataUpdate(data[0], data[1], data[2])
                graph2.dataUpdate(data[3], data[4], data[5])
            }
        }
        function onScanningFinished() {
            listView.enabled=true
            scanButton.enabled=true
            scanButton.text="Scan"
            listView.enabled=true
            busyIndicator.running=false
            scanButton.enabled=true
            console.log("ScanningFinished")
        }
        function onConnectionStart() {
            busyIndicator.running=false
            drawer.close()
            graph.dataClear()
            graph2.dataClear()
            refreshTimer.start()
            console.log("ConnectionStart")
        }
    }
}
