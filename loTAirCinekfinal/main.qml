import QtQuick 2.15
import QtQuick.Controls 2.15
import QtGraphicalEffects 1.12
import QtCharts 2.15
ApplicationWindow {
    width: 640
    height: 480
    visible: true
    title: qsTr("IoT Air")
    Frame {
        id: frameText
        height: 30
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.right: parent.right
        anchors.rightMargin: 10
        Text {
            id: textInfo
            anchors.centerIn: parent
            color: "#ffffff"
            text: qsTr("")
            font.pixelSize: 15
        }
    }
    Frame {
        id: frameGauge
        height: width/3
        anchors.top: frameText.bottom
        anchors.topMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.right: parent.right
        anchors.rightMargin: 10
        Gauge {
            id: gaugePressure
            anchors.centerIn: parent
            anchors.horizontalCenterOffset: -0.3*parent.width
            size: 0.6*parent.height
            colorCircle: "#00C0ff"
            colorBackground: "#202020"
            lineWidth: 0.1*width
            showBackground: true
            unit: "hPa"
            from:800
            to:1200
            Text {
                id: textPressure
                anchors.centerIn: parent
                anchors.verticalCenterOffset: parent.height/2+height
                color: parent.colorCircle
                text: qsTr("Pressure")
                font.pixelSize: 0.15*parent.width
            }
        }
        Gauge {
            id: gaugeTemperature
            anchors.centerIn: parent
            anchors.verticalCenterOffset: 0
            anchors.horizontalCenterOffset: 0
            size: 0.7*parent.height
            colorCircle: "#ff8000"
            colorBackground: "#202020"
            lineWidth: 0.15*width
            showBackground: true
            unit: "C"
            from:0
            to:85
            Text {
                id: textTemperature
                anchors.centerIn: parent
                anchors.verticalCenterOffset: parent.height/2+height
                color: parent.colorCircle
                text: qsTr("Temperature")
                font.pixelSize: 0.15*parent.width
            }
        }
        Gauge {
            id: gaugeHumidity
            anchors.centerIn: parent
            anchors.horizontalCenterOffset: 0.3*parent.width
            size: 0.6*parent.height
            colorCircle: "#00ffc0"
            colorBackground: "#202020"
            lineWidth: 0.1*width
            showBackground: true
            unit: "%"
            from:10
            to:100
            Text {
                id: textHumidity
                anchors.centerIn: parent
                anchors.verticalCenterOffset: parent.height/2+height
                color: parent.colorCircle
                text: qsTr("Humidity")
                font.pixelSize: 0.15*parent.width
            }
        }
        Glow {
            id:glowPressure
            anchors.fill: gaugePressure
            radius: 25
            samples: 40
            spread: 0.0
            color: "#00C0ff"
            source: gaugePressure
        }
        Glow {
            id:glowTemperature
            anchors.fill: gaugeTemperature
            radius: 35
            samples: 40
            spread: 0.0
            color: "#ff8000"
            source: gaugeTemperature
        }
        Glow {
            id:glowHumidity
            anchors.fill: gaugeHumidity
            radius: 25
            samples: 40
            spread: 0.0
            color: "#00ffc0"
            source: gaugeHumidity
        }
    }
}
